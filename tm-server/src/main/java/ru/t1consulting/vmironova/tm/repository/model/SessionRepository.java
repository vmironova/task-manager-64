package ru.t1consulting.vmironova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1consulting.vmironova.tm.model.Session;
import ru.t1consulting.vmironova.tm.model.User;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface SessionRepository extends AbstractUserOwnedRepository<Session> {

    long countByUser(@NotNull final User user);

    void deleteByUserAndId(@NotNull final User user, @NotNull final String id);

    @NotNull
    List<Session> findByUser(@NotNull final User user);

    @NotNull
    List<Session> findByUser(@NotNull final User user, @NotNull final Sort sort);

    @NotNull
    Optional<Session> findByUserAndId(@NotNull final User user, @NotNull final String id);

    void deleteByUser(@NotNull final User user);

    @Query("select case when count(c)> 0 then true else false end from Session c where user.id = :userId and id = :id")
    boolean existByUserIdAndId(@Param("userId") String userId, @Param("id") String id);

}
