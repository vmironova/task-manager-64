package ru.t1consulting.vmironova.tm.api;

import org.jetbrains.annotations.NotNull;

public interface FieldConstants {

    @NotNull
    String COLUMN_NAME = "name";

    @NotNull
    String COLUMN_CREATED = "created";

    @NotNull
    String COLUMN_STATUS = "status";

}
