package ru.t1consulting.vmironova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1consulting.vmironova.tm.enumerated.CustomSort;

import java.util.List;

public interface IUserOwnedDTOService<M extends AbstractUserOwnedModelDTO> extends IDTOService<M> {

    @NotNull
    M add(@Nullable String userId, @NotNull M model) throws Exception;

    void clear(@Nullable String userId) throws Exception;

    @Nullable
    List<M> findAll(@Nullable String userId) throws Exception;

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable CustomSort sort) throws Exception;

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    void remove(@Nullable String userId, @Nullable M model) throws Exception;

    void removeById(@Nullable String userId, @Nullable String id) throws Exception;

    void update(@Nullable String userId, @Nullable M model) throws Exception;

}
